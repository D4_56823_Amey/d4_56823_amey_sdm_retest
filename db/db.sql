CREATE table movies (
    movie_id int Primary key auto_increment, 
    movie_title VARCHAR(100), 
    movie_release_date DATE,
    movie_time TIME,
    director_name VARCHAR(50)
)

